module NodeHelpers exposing (mapElements)

import Html.Parser exposing (Node(..))
import List.Extra as ListX


mapElements : (List Int -> String -> List ( String, String ) -> a) -> List Node -> List a
mapElements mapFn nodes =
    pathedMapElements mapFn [] [] nodes


pathedMapElements : (List Int -> String -> List ( String, String ) -> a) -> List Int -> List a -> List Node -> List a
pathedMapElements mapFn prevPath acc nodes =
    ListX.indexedFoldl
        (\idx node mapped ->
            case node of
                Element tag attributes children ->
                    let
                        curPath =
                            idx :: prevPath
                    in
                    mapFn curPath tag attributes
                        :: pathedMapElements mapFn curPath mapped children

                _ ->
                    mapped
        )
        acc
        nodes
        |> List.reverse
