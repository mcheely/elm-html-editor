module Main exposing (main)

import Browser
import Dict exposing (Dict)
import Html exposing (Html, div, label, option, select, span, text, textarea)
import Html.Attributes exposing (class, contenteditable, value)
import Html.Events exposing (onInput)
import Html.Parser exposing (Attribute, Node(..), nodeToString)
import Html.Parser.Util exposing (toVirtualDom)
import Http
import Json.Decode as Json
import List.Extra as ListX
import NodeHelpers as Node
import Parser exposing (DeadEnd, deadEndsToString)



-- MODEL


type alias Model =
    { text : String
    , problems : List DeadEnd
    , parsedHtml : List Node
    }


startingText =
    """
<h1>Title</h1>
<div>
  <span>
    <p>
       <input type="text"/>
    </p>
  </span>
</div>
<button type="submit">Test</button>
    """ |> String.trim


init : () -> ( Model, Cmd Msg )
init _ =
    let
        ( parsed, errors ) =
            case Html.Parser.run startingText of
                Ok nodes ->
                    ( nodes, [] )

                Err problems ->
                    ( [], problems )
    in
    ( { text = startingText
      , problems = errors
      , parsedHtml = parsed
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = TextEdited String
    | SetAttribute (List Int) String String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TextEdited htmlText ->
            let
                ( parsed, errors ) =
                    case Html.Parser.run htmlText of
                        Ok nodes ->
                            ( nodes, [] )

                        Err problems ->
                            ( model.parsedHtml, problems )
            in
            ( { model
                | problems = errors
                , parsedHtml = parsed
              }
            , Cmd.none
            )

        SetAttribute path attr val ->
            let
                newNodes =
                    setNodeAttr path attr val model.parsedHtml
            in
            ( { parsedHtml = newNodes
              , problems = model.problems
              , text =
                    List.map nodeToString newNodes
                        |> String.concat
              }
            , Cmd.none
            )


setNodeAttr : List Int -> String -> String -> List Node -> List Node
setNodeAttr path attr val nodes =
    case path of
        [ targetIdx ] ->
            ListX.updateAt targetIdx (setElementAttr attr val) nodes

        next :: rest ->
            ListX.updateAt next (setChildNodeAttr rest attr val) nodes

        [] ->
            Debug.todo "Shouldn't get here!"


setChildNodeAttr : List Int -> String -> String -> Node -> Node
setChildNodeAttr path attr val node =
    case node of
        Element tag attributes children ->
            Element tag attributes (setNodeAttr path attr val children)

        _ ->
            node


setElementAttr : String -> String -> Node -> Node
setElementAttr attr val node =
    case node of
        Element tag attributes children ->
            Element tag (updateAttr attr val attributes) children

        _ ->
            node


updateAttr : String -> String -> List Attribute -> List Attribute
updateAttr targetAttr newValue attributes =
    List.foldl
        (\( attr, val ) ( newAttrs, found ) ->
            if attr == targetAttr then
                ( ( attr, newValue ) :: newAttrs, True )

            else
                ( ( attr, val ) :: newAttrs, found )
        )
        ( [], False )
        attributes
        |> (\( attrs, replaced ) ->
                if replaced then
                    List.reverse attrs

                else
                    List.reverse (( targetAttr, newValue ) :: attrs)
           )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ div [ class "editor" ]
            [ inputView model.text
            , parsedViews model.parsedHtml
            ]
        , div [ class "errors" ]
            [ case model.problems of
                [] ->
                    text ""

                errors ->
                    text (deadEndsToString errors)
            ]
        ]


inputView : String -> Html Msg
inputView htmlText =
    aceEditor
        [ class "input"
        , theme "ace/theme/monokai"
        , mode "ace/mode/html"
        , onEdit TextEdited
        ]
        [ text htmlText ]


aceEditor : List (Html.Attribute msg) -> List (Html msg) -> Html msg
aceEditor =
    Html.node "juicy-ace-editor"


theme : String -> Html.Attribute msg
theme =
    Html.Attributes.attribute "theme"


onEdit : (String -> msg) -> Html.Attribute msg
onEdit tag =
    Html.Events.on "change"
        (Json.at [ "target", "value" ] Json.string
            |> Json.map tag
        )


mode : String -> Html.Attribute msg
mode =
    Html.Attributes.attribute "mode"


parsedViews : List Node -> Html Msg
parsedViews nodes =
    div [ class "parsed-views" ]
        [ renderedView nodes
        , structuredEditor nodes
        ]


renderedView : List Node -> Html Msg
renderedView nodes =
    div [ class "rendered" ] (toVirtualDom nodes)


structuredEditor : List Node -> Html Msg
structuredEditor nodes =
    div [ class "structured-editor" ] (Node.mapElements (elementEditor nodes) nodes)


elementEditor : List Node -> List Int -> String -> List ( String, String ) -> Html Msg
elementEditor nodes path tag attributes =
    case Dict.get tag tagConfigs of
        Nothing ->
            text ""

        Just attrConfig ->
            attributesEditor nodes attrConfig path tag attributes


attributesEditor :
    List Node
    -> List ( String, List String )
    -> List Int
    -> String
    -> List ( String, String )
    -> Html Msg
attributesEditor nodes config path tag attributes =
    let
        attrDict =
            Dict.fromList attributes

        fwPath =
            List.reverse path
    in
    div [ class "element-editor" ]
        [ div [ class "element-path" ] (tagPath nodes fwPath [])
        , div [ class "attribute-editors" ]
            (List.map (attributeEditor fwPath attrDict) config)
        ]


attributeEditor : List Int -> Dict String String -> ( String, List String ) -> Html Msg
attributeEditor path attrs ( targetAttr, options ) =
    div [ class "attribute-editor" ]
        [ label []
            [ text targetAttr
            , text " "
            , select
                [ value (Dict.get targetAttr attrs |> Maybe.withDefault "")
                , onInput (SetAttribute path targetAttr)
                ]
                (List.map
                    (\attrOpt ->
                        option [ value attrOpt ] [ text attrOpt ]
                    )
                    options
                )
            ]
        ]


tagPath : List Node -> List Int -> List (Html Msg) -> List (Html Msg)
tagPath nodes path acc =
    case path of
        cur :: rest ->
            case ListX.getAt cur nodes of
                Just node ->
                    case node of
                        Element tag _ children ->
                            tagPath children rest (span [] [ text tag ] :: acc)

                        _ ->
                            [ text "?" ]

                Nothing ->
                    [ text "?" ]

        [] ->
            List.reverse acc


tagConfigs =
    Dict.fromList
        [ ( "input", [ ( "type", [ "text", "password", "number" ] ) ] )
        , ( "button", [ ( "type", [ "submit", "button" ] ) ] )
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }
